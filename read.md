# Correction TP3
## Branche : step1-blueprint_it

[+] sous modules 'products' et 'productsLine'

[+] ajout d'une produit et flux :
2 'trucs' importants :
* J'utilise la même 'vue' que pour l'édition mais je lui passe en paramètre si je suis en mode ajout ou modification
* Pour ne pas que l'utilisateur perde sa saise en cas d'erreur, on reconstruit le dictionnaire que la vue a besoin 'comme pour l'édition' 
````python
        else:
            # Pour éviter que l'utilisateur ne perde toute sa saisie :/
            productDetails = {
                'productCode': productCode,
                'productName': request.form['productName'],
                'productLine': request.form['productLine'],
                'productScale': request.form['productScale'],
                'productDescription': request.form['productDescription'],
                'buyPrice': request.form['buyPrice'],
                'MSRP': request.form['MSRP'],
                'quantityInStock': request.form['quantityInStock'],
            }
````


### Notes EC:
Penser à faire ->
````shell
pip freeze > requirements.txt
````
