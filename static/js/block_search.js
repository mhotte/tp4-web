"use strict";

$(document).ready(function () {
    let inpSearch = document.getElementById("search");
    inpSearch.style.border = "1px solid lightgray";
    inpSearch.addEventListener('input', function () {
        inpSearch.classList.add("disabled");
        let valSearch = inpSearch.value;
        let boxSearch = document.getElementById("box-search");
        if (valSearch.length > 0) {
            $.ajax({
                type: "POST",
                url: '/api/getSearchProducts',
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                data: JSON.stringify({'search_criteria': valSearch}),
                success: function (data) {
                    let searchContainer = document.getElementById("search-container");

                    if (searchContainer.hasChildNodes()) {
                        while (searchContainer.lastChild) {
                            searchContainer.removeChild(searchContainer.lastChild);
                        }
                    }

                    data = JSON.parse(data);
                    if (data.hasOwnProperty('nb_results') && data.nb_results > 0) {
                        let products = data.products;

                        products.forEach(function (productLines) {
                            $(searchContainer).append('<span style="border:2px solid gray; width:100%;">ProductLine : ' + productLines.lineName + '</span>');

                            productLines.products.forEach(function (product) {
                                let $divProduct = $('<a class="d-flex btn btn-outline-secondary" href="/product/details/' + product.productCode + '" style="background: white;"></a>');
                                $divProduct.append('<img src="' + product.productImg + '" alt="Image du produit ' + product.productCode + '" style="max-width: 4rem; max-height: 4rem;"/>');
                                $divProduct.append('<a href="/product/details/' + product.productCode + '">' + product.productName + '</a>');

                                $(searchContainer).append($divProduct);
                            });
                        });
                        document.getElementById("nb-results").textContent = data.nb_results + "produits trouvés";
                    } else {
                        document.getElementById("nb-results").textContent = "Aucun produit trouvé";
                    }
                    boxSearch.classList.remove("hide");
                    boxSearch.classList.add("show");
                },
                error: function (e) {
                    window.console.log(e);
                    alert('Error Ajax : ' + e);
                }
            });
        } else {
            boxSearch.classList.remove("show");
            boxSearch.classList.add("hide");
        }
        inpSearch.classList.remove("disabled");
    }, false);
});

