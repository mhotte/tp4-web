from flask import Blueprint, render_template, request, redirect, session, g, jsonify, abort
import simplejson as json
from dataModules.store import products
from dataModules.store import orders

api = Blueprint('api', __name__,
                url_prefix='/api',
                )
products = products.product()
orders = orders.order()


@api.route('/')
def default():
    return 'Hello world'


@api.route('/getQuote', methods=['POST'])
def get_quote():
    #
    # On reçoit un contenu de panier directement au format Json grâce au contentType 'application/json' dans l'appel Ajax:
    #
    # print(json.dumps(request.json))
    dic_cart = request.json
    # print(dic_cart.keys())
    # for key in dic_cart:
    #     print(dic_cart[key])

    resp = products.get_quote(dic_cart)
    # print(resp)

    return json.dumps(resp)


@api.route('/placeOrder', methods=['POST'])
def place_order():
    if g.user['user_name'] == 'Guest':
        return json.dumps({'error': True, 'message': 'Utilisateur inconnu'})

    return json.dumps(products.place_order(request.json, g.user['user_id']))


@api.route('/getOrder', methods=['GET'])
def get_order():
    if g.user['user_name'] == 'Guest':
        return abort(400)

    results = jsonify(orders.get_order_history(g.user['user_id']))
    # Pour test
    j_results = json.dumps(results.response)
    return j_results


@api.route('/getSearchProducts', methods=['POST'])
def get_search_products():
    resp = products.nav_search_products_(request.json['search_criteria'])
    results = jsonify(resp)
    # Pour test
    j_result = json.dumps(results.response)
    return j_result
