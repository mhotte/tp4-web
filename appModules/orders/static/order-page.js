'use strict';

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: '/api/getOrder',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        success: function (data) {

            if (data.error) {

                $('#div-order-error').text(data.message).fadeIn();
                console.log(data);
                // alert('Erreur :' + data.message);

            } else {
                data = JSON.parse(data);
                var $table = $('<div/>').addClass('container');
                // $('#cartInStockTable').append('<div>');

                let $row = $('<div/>').addClass('row');
                $row.append('<div class="col-md-2 fw-bolder">Numéro de commande</div>');
                $row.append('<div class="col-md-2 fw-bolder"><p class="text-end">Status</p></div>');
                $row.append('<div class="col-md-2 fw-bolder"><p class="text-end">Date de l\'achat</p></div>');
                $row.append('<div class="col-md-2 fw-bolder"><p class="text-end">Required date</p></div>');
                $row.append('<div class="col-md-2 fw-bolder"><p class="text-end">Shipped date</p></div>');
                $row.append('<div class="col-md-2 fw-bolder"><p class="text-end">Commentaires</p></div>');
                $table.append($row);

                data.forEach(function (order) {
                    let orderDate = 'Aucune';
                    let requiredDate = 'Aucune';
                    let shippedDate = 'Aucune';
                    let comments = order.comments || 'Aucun';
                    if (order.orderDate) {
                        orderDate = new Date(order.orderDate);
                        orderDate = orderDate.getDay() + ' ' + orderDate.toLocaleString('default', {month: 'long'}) + ' ' + orderDate.getFullYear();
                    }
                    if (order.requiredDate) {
                        requiredDate = new Date(order.requiredDate);
                        requiredDate = requiredDate.getDay() + ' ' + requiredDate.toLocaleString('default', {month: 'long'}) + ' ' + requiredDate.getFullYear();

                    }
                    if (order.shippedDate) {
                        shippedDate = new Date(order.shippedDate);
                        requiredDate = shippedDate.getDay() + ' ' + shippedDate.toLocaleString('default', {month: 'long'})+ ' ' + shippedDate.getFullYear();

                    }

                    let $row = $('<div/>').addClass('row');
                    $row.append('<div class="col-md-2"><a href="/order/' + order.orderNumber + '">' + order.orderNumber + '</a></div>');
                    $row.append('<div class="col-md-2"><p class="text-end">' + order.status + '</p></div>');
                    $row.append('<div class="col-md-2"><p class="text-end">' + orderDate + '</p></div>');
                    $row.append('<div class="col-md-2"><p class="text-end">' + requiredDate + '</p></div>');
                    $row.append('<div class="col-md-2"><p class="text-end">' + shippedDate + '</p></div>');
                    $row.append('<div class="col-md-2"><p class="text-end">' + comments + '</p></div>');
                    $table.append($row);
                });

                $('#div-order-content').append($table);
            }
        },
        error: function (e) {
            console.log(e);
            alert('Error Ajax : ' + e);
        }
    });
});
