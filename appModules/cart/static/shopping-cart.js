"use strict";

var shoppingCartManagement = shoppingCartManagement || {};

shoppingCartManagement = (function () {

    var shoppingCartDiv = null;

    //
    // Affiche et masque la DIV pour le panier d'achat
    //
    function showShoppingDiv() {
        if (shoppingCartDiv.classList.contains('hide')) {
            shoppingCartDiv.classList.remove('hide');
        }
    }

    function hideShoppingDiv() {
        if (!shoppingCartDiv.classList.contains('hide')) {
            shoppingCartDiv.classList.add('hide');
        }
    }

    function toggleShoppingDiv(event) {
        if (shoppingCartDiv.classList.contains('hide')) {
            shoppingCartDiv.classList.remove('hide');
            drawShoppingCart();
        } else {
            shoppingCartDiv.classList.add('hide');
        }
    }

    function getLocalCart() {
        var localCart = {};
        if (localStorage.getItem("cart") !== null) {
            localCart = JSON.parse(localStorage.getItem('cart'));
        }
        return localCart;
    }

    function saveLocalCart(localCart) {
        localStorage.setItem('cart', JSON.stringify(localCart));
    }

    //
    // Ajoute un item au panier d'achat
    //
    function addShoppingCart(event) {

        console.log(event.target.dataset);

        var productCode = event.target.dataset['productcode'];
        var productName = event.target.dataset['productname'];
        var productImg = event.target.dataset['productimg'];

        //
        var localCart = getLocalCart();
        // console.log(localCart);

        // Est-ce que le produit est déjà présent dans notre localStore?
        if (localCart[productCode]) {
            // oui: on ajout 1 à la quantité
            var productQuantity = localCart[productCode]['quantity'];
            localCart[productCode]['quantity'] = productQuantity + 1;
        } else {
            // non: On le créer et on met 1 en quantité
            localCart[productCode] = {
                productCode: productCode,
                productName: productName,
                productImg: productImg,
                quantity: 1,
            };
        }

        //
        // On rafraîchis l'affichage du Cart
        //

        showShoppingDiv();
        drawShoppingCart(localCart);

        //
        // On sauvegarde le localStorage
        //
        saveLocalCart(localCart);
    }

    //
    // Enlève 1 à un produit dans le panier. (id du produit passé par le data-productcode)
    //
    function decrementQuantity(event) {
        var productCode = event.target.dataset['productcode'];
        var localCart = getLocalCart();
        if (localCart[productCode]) {
            var qty = localCart[productCode]['quantity'];
            if (qty - 1 > 0) {
                localCart[productCode]['quantity'] = qty - 1;
            } else {
                localCart[productCode]['quantity'] = 0;
            }
        }
        //
        // Rafraîchis le panier local (localStorage) ET rafraîchis l'affichage
        saveLocalCart(localCart);
        drawShoppingCart(localCart);
    }

    function incrementQuantity(event) {
        var productCode = event.target.dataset['productcode'];
        var localCart = getLocalCart();
        if (localCart[productCode]) {
            var qty = localCart[productCode]['quantity'];
            localCart[productCode]['quantity'] = qty + 1;
        }
        //
        //
        saveLocalCart(localCart);
        drawShoppingCart(localCart);
    }

    function removeFromCart(event) {
        var productCode = event.target.dataset['productcode'];
        var localCart = getLocalCart();

        delete localCart[productCode];

        //
        //
        saveLocalCart(localCart);
        drawShoppingCart(localCart);
    }

    //
    // Technique #1 : Permet de générer le petit bloc du panier d'achat
    //
    function drawShoppingCart(localCart = null) {

        if (!localCart) {
            if (localStorage.getItem("cart") !== null) {
                localCart = JSON.parse(localStorage.getItem('cart'));
            }
        }

        console.log(localCart);

        var shoppingCartDivContent = document.getElementById("shoppingCartDivContent");

        var newCartcontainer = document.createElement('div');
        newCartcontainer.classList.add('container');

        var rows = [];

        var nbProdInCart = Object.keys(localCart).length;

        if (nbProdInCart === 0) {
            var aRow = document.createElement('div');
            aRow.classList.add('empty-cart');
            aRow.innerText = 'Aucun produit dans votre panier.';
            newCartcontainer.appendChild(aRow);

        } else {
            Object.keys(localCart).forEach(function (productCode) {

                var aProduct = localCart[productCode];

                var aRow = document.createElement('div');
                aRow.classList.add('row');

                // Image
                var aColImg = document.createElement('div');
                aColImg.classList.add('col-md-2');
                var pImg = document.createElement('img');
                addMultipleClass(pImg, ['small-cart-image'])
                pImg.src = aProduct.productImg;

                aColImg.appendChild(pImg);

                aRow.appendChild(aColImg);

                // Titre
                var aColName = document.createElement('div');
                aColName.classList.add('col-md-5');

                var aLinkToProduct = document.createElement('a');
                aLinkToProduct.href = '/product/details/' + aProduct.productCode;
                aLinkToProduct.innerText = aProduct.productName;

                aColName.appendChild(aLinkToProduct);

                aRow.appendChild(aColName);

                // [-]
                var aColMinus = document.createElement('div');
                aColMinus.classList.add('col-md-1');

                var btnMinus = document.createElement('div');
                btnMinus.classList.add('btn-cart-minus');
                // btnMinus.innerText = '[-]';
                btnMinus.dataset.productcode = aProduct.productCode;
                // On pourrais ajouter le listener directement ici, je les fais tous plus bas...
                // btnMinus.addEventListener('click', decrementQuantity, false);
                aColMinus.appendChild(btnMinus);

                aRow.appendChild(aColMinus);

                // {Quantité}
                var aColQu = document.createElement('div');
                aColQu.classList.add('col-md-1');
                aColQu.innerText = aProduct.quantity;

                aRow.appendChild(aColQu);

                // [+]
                var aColPls = document.createElement('div');
                aColPls.classList.add('col-md-1');
                //
                var btnPlus = document.createElement('div');
                btnPlus.classList.add('btn-cart-plus');
                // btnPlus.innerText = '[+]';
                btnPlus.dataset.productcode = aProduct.productCode;
                aColPls.appendChild(btnPlus);
                //
                aRow.appendChild(aColPls);

                // [Suppr]
                var aColSupp = document.createElement('div');
                aColSupp.classList.add('col-md-2');

                var btnDel = document.createElement('div');
                btnDel.classList.add('btn-cart-del');
                // btnDel.innerText = 'X';
                btnDel.dataset.productcode = aProduct.productCode;
                aColSupp.appendChild(btnDel);

                aRow.appendChild(aColSupp);

                newCartcontainer.appendChild(aRow);

            });
        }

        shoppingCartDivContent.textContent = '';
        shoppingCartDivContent.appendChild(newCartcontainer);

        // On vient réaffecter les différents évènements aux objets créés
        var allBtnMinus = document.getElementsByClassName('btn-cart-minus');
        allBtnMinus.forEach(function (element) {
            element.addEventListener('click', decrementQuantity, false);
        });
        var allBtnPlus = document.getElementsByClassName('btn-cart-plus');
        allBtnPlus.forEach(function (element) {
            element.addEventListener('click', incrementQuantity, false);
        });
        var allBtnDel = document.getElementsByClassName('btn-cart-del');
        allBtnDel.forEach(function (element) {
            element.addEventListener('click', removeFromCart, false);
        });

        // shoppingCartDivContent.innerHTML = JSON.stringify(localCart);

    }


    //
    // Gestion du panier d'achat
    //
    function initialisation() {
        console.log('shoppingCartManagement initialisation.');

        shoppingCartDiv = document.getElementById("shoppingCartDiv");

        document.getElementById("shoppingCartButton").addEventListener('click', toggleShoppingDiv, false);

        //
        // On ajoute un évènement sur tous les 'bouton' dont la classe est 'addToShoppingCart.
        // Ainsi on sera capable dans les listes d'avoir un bouton 'ajouter'
        //
        var allAddToShoppingCartButtons = document.getElementsByClassName('add-to-shopping-cart');
        allAddToShoppingCartButtons.forEach(function (element) {
            element.addEventListener('click', addShoppingCart, false);
        });
    }

    //
    //
    //

    //
    // On 'expose' les éléments isolés dans la fonction
    //
    return {
        initialisation: initialisation,
    };
    //
    //
    //
})();

function addMultipleClass(element, classes) {
    classes.forEach(function (aClass) {
        console.log(aClass);
        element.classList.add(aClass);
    });
}

window.addEventListener("load", shoppingCartManagement.initialisation, false);
