import slug
from flask import Blueprint, render_template, request, redirect, g, abort

from dataModules.store import products, productLines

products = products.product()
productLines = productLines.productLine()

product = Blueprint('product', __name__, url_prefix='/product', template_folder='templates')


@product.route('/details/<productCode>')
def product_details(productCode):
    return render_template('productDetails.html', product=products.get_details(productCode))


@product.route('/edit/<productCode>', methods=['POST', 'GET'])
def product_edit(productCode):
    #
    # Test si l'utilisateur a la droit !
    #
    if not g.user['is_employee']:
        return abort(403)
    #
    #
    #

    errors = False
    updateOk = False
    if request.method == 'POST':
        #
        # Appel à l'update dans la classe
        #
        errors = products.update_product(request, productCode)
        if not errors:
            updateOk = True

    return render_template('productEdit.html',
                           content={
                               "details": products.get_details(productCode),
                               "insertMode": False,
                               "productScales": products.get_product_scales(),
                               "productLines": [row['productLine'] for row in productLines.get_product_lines()],
                               "productVendors": products.get_product_vendors(),
                               "errors": errors,
                               "updateOk": updateOk,
                           })


@product.route('/add', methods=['POST', 'GET'])
def product_add():
    #
    # Test si l'utilisateur a la droit !
    #
    if not g.user['is_employee']:
        return abort(403)
    #
    #
    #

    errors = False
    updateOk = False
    productDetails = {}
    if request.method == 'POST':
        #
        # Appel à l'insert dans la classe
        #
        productCode = slug.slug(request.form['productCode'])
        errors = products.insert_product(request, productCode)
        if not errors:
            # Si l'ajout est ok, on redirige vers la page d'édition.
            return redirect('/product/edit/' + productCode)
        else:
            # Pour éviter que l'utilisateur ne perde toute sa saisie :/
            productDetails = {
                'productCode': productCode,
                'productName': request.form['productName'],
                'productLine': request.form['productLine'],
                'productScale': request.form['productScale'],
                'productDescription': request.form['productDescription'],
                'buyPrice': request.form['buyPrice'],
                'MSRP': request.form['MSRP'],
                'quantityInStock': request.form['quantityInStock'],
            }

    return render_template('productEdit.html',
                           content={
                               "details": productDetails,
                               "insertMode": True,
                               "productScales": products.get_product_scales(),
                               "productLines": [row['productLine'] for row in productLines.get_product_lines()],
                               "productVendors": products.get_product_vendors(),
                               "errors": errors,
                               "updateOk": updateOk,
                           })
