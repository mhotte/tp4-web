"use strict";

var createAccount = createAccount || {};

createAccount = (function () {

    var regexCourriel = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var errors = {};

    //
    // Abstraction du formulaire Html, avec des indicaiton pour sa validation.
    //
    var metaForm = [
        {
            formId: 'username',
            label: 'Nom utilisateur',
            required: true,
        },
        {
            formId: 'passwordA',
            label: 'Mot de passe',
            required: true,
        },
        {
            formId: 'firstName',
            label: 'Prénom',
            required: true,
            minChar: 3,
            maxChar: 50,
        },
        {
            formId: 'lastName',
            label: 'Nom',
            required: true,
            minChar: 3,
            maxChar: 50,
        },
        {
            formId: 'addressLine1',
            label: 'Adresse',
            required: true,
            minChar: 3,
            maxChar: 50,
        },
        {
            formId: 'addressLine2',
            label: 'Adresse',
            required: false,
            minChar: 3,
            maxChar: 50,
        },
        {
            formId: 'city',
            label: 'Ville',
            required: true,
            minChar: 3,
            maxChar: 50,
        },
        {
            formId: 'state',
            label: 'État',
            required: false,
            minChar: 3,
            maxChar: 50,
        },
    ];

    function addError(errorsArray, name, value) {
        if (!errorsArray[name]) {
            errorsArray[name] = [];
        }
        errorsArray[name].push(value);
    }

    function reinitErrorMessages(metaInfos) {
        metaInfos.forEach(function (aMeta) {
            var errorLabelEl = document.getElementById('error-' + aMeta.formId);
            if (errorLabelEl) {
                errorLabelEl.textContent = '';
            }
        });
    }

    function displayErrorMessages(metaInfos, errorsArray) {
        metaInfos.forEach(function (aMeta) {
            var errorLabelEl = document.getElementById('error-' + aMeta.formId);
            console.log('error-' + aMeta.formId, errorLabelEl);
            if (errorLabelEl) {
                if (errorsArray[aMeta.formId]) {
                    errorLabelEl.textContent = errorsArray[aMeta.formId].join(', ');
                }
            }
        });
    }

    function genericValidForm(metaInfos, errorsArray) {

        reinitErrorMessages(metaInfos);

        metaInfos.forEach(function (aMeta) {

            if (document.getElementById(aMeta.formId)) {

                var toCheck = document.getElementById(aMeta.formId).value.trim();
                if (aMeta.required) {
                    if (toCheck.length === 0) {
                        addError(errorsArray, aMeta.formId, aMeta.label + ' est obligatoire')
                    }
                }
                if (aMeta.required || toCheck.length > 0) {
                    if (aMeta.minChar) {
                        if (toCheck.length < aMeta.minChar) {
                            addError(errorsArray, aMeta.formId, aMeta.label + ' doit avoir au moins ' + aMeta.minChar + ' caractères');
                        }
                    }
                    if (aMeta.maxChar) {
                        if (toCheck.length > aMeta.maxChar) {
                            addError(errorsArray, aMeta.formId, aMeta.label + ' doit avoir au plus ' + aMeta.maxChar + ' caractères');
                        }
                    }
                }

            } else {
                console.log('Meta invalid-formId not found', aMeta);
            }

        });

    }

    function beforeSubmit(event) {

        errors = {};

        console.log('beforeSubmit');

        //
        // Username (Spécial)
        //
        var userName = document.getElementById("username").value.trim();
        if (!regexCourriel.test(userName)) {
            addError(errors, 'username', 'Le courriel doit être valide');
        }
        if (userName.length > 50) {
            addError(errors, 'username', 'Nom d\'utilisateur pas plus de 50 caractères');
        }

        //
        // Concordance mots de passe
        //
        var passWordA = document.getElementById("passwordA").value.trim();
        var passWordB = document.getElementById("passwordB").value.trim();

        if(passWordA !== passWordB) {
            addError(errors, 'passwordA', 'Les mots de passe ne correspondent pas.');
        }

        //
        // Validation générique
        //
        genericValidForm(metaForm, errors);

        //
        // Une erreur s'est produite : on bloque l'envoi du formulaire ?
        //
        displayErrorMessages(metaForm, errors);
        if (Object.keys(errors).length > 0) {
            console.log(errors);
            event.preventDefault();

        }


    }

    //
    // Gestion du formulaire de creation de compte
    //
    function initialisation() {
        console.log('createAccount management initialisation.');
        document.getElementById("createAccountForm").addEventListener('submit', beforeSubmit, false);
    }

    //
    // On 'expose' les éléments isolés dans la fonction
    //
    return {
        initialisation: initialisation,
    };
})();

window.addEventListener("load", createAccount.initialisation, false);
