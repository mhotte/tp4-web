from sqlalchemy import Integer, DateTime, INT, DECIMAL, SMALLINT, VARCHAR, String
from sqlalchemy import select, asc, create_engine, Column, or_, desc
# from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import declarative_base, column_property
from sqlalchemy.orm import sessionmaker

engine = create_engine('mysql+mysqldb://root:@localhost/classicmodels')

Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

Base = declarative_base()


class Orders_Table(Base):
    __tablename__ = 'orders'
    orderNumber = Column('orderNumber', Integer, primary_key=True)
    orderDate = column_property(Column('orderDate', DateTime))
    requiredDate = column_property(Column('requiredDate', DateTime))
    shippedDate = column_property(Column('shippedDate', DateTime))
    status = column_property(Column('status', VARCHAR))
    comments = column_property(Column('comments', String))
    customerNumber = column_property(Column('customerNumber', INT))

    def __repr__(self):
        return "%s, %s, %s, %s, %s, %s" % (
            self.orderNumber,
            self.orderDate,
            self.requiredDate,
            self.shippedDate,
            self.status,
            self.comments
        )

    def get_orders(self, user_id):
        order_statement = select(Orders_Table).where(Orders_Table.customerNumber == user_id)
        results = []
        for row in session.execute(order_statement):
            for order in row:
                results.append({
                    'orderNumber': order.orderNumber,
                    'orderDate': order.orderDate,
                    'requiredDate': order.requiredDate,
                    'shippedDate': order.shippedDate,
                    'status': order.status,
                    'comments': order.comments
                })
        return results


class Products_Table(Base):
    __tablename__ = 'products'
    productCode = Column(Integer, primary_key=True)
    productName = column_property(Column('productName', VARCHAR(70)))
    productLine = column_property(Column('productLine', VARCHAR(50)))
    productScale = column_property(Column('productScale', VARCHAR(10)))
    productVendor = column_property(Column('productVendor', VARCHAR(50)))
    productDescription = column_property(Column('productDescription', String))
    quantityInStock = column_property(Column('quantityInStock', SMALLINT))
    buyPrice = column_property(Column('buyPrice', DECIMAL(10, 2)))
    MSRP = column_property(Column('MSRP', DECIMAL(10, 2)))

    def __repr__(self):
        return "%s, %s, %s, %s, %s, %s, %s, %s, %s" % (
            self.productCode,
            self.productName,
            self.productLine,
            self.productScale,
            self.productVendor,
            self.productDescription,
            self.quantityInStock,
            self.buyPrice,
            self.MSRP
        )

    def nav_search(self, search_criteria):
        search_criteria = '%{}%'.format(search_criteria)
        search_statement = select(
            Products_Table.productCode,
            Products_Table.productName,
            Products_Table.productLine
        ).where(
            or_(
                Column.like(Products_Table.productName, search_criteria),
                Column.like(Products_Table.productDescription, search_criteria),
            )
        ).order_by(
            asc(Products_Table.productLine)
        )

        # print(search_statement)

        results = []
        for row in session.execute(search_statement):
            # print(row)
            results.append({
                'productCode': row.productCode,
                'productName': row.productName,
                'productLine': row.productLine,
                'productImg': f"/static/uploads/{row.productCode}.jpg"
            })

        return {'products': results, 'nb_results': len(results)}

    def get_top_10(self):
        search_statement = select(
            Products_Table.productCode,
            Products_Table.productName,
            Products_Table.productLine,
            Products_Table.productDescription
        ).order_by(
            desc(Products_Table.quantityInStock)
        ).limit(10)

        results = []
        for row in session.execute(search_statement):
            # print(row)
            results.append({
                'productCode': row.productCode,
                'productName': row.productName,
                'productLine': row.productLine,
                'productDescription': row.productDescription,
                'productImg': f"/static/uploads/{row.productCode}.jpg"
            })

        return results
