import os
import os.path

from flask import g

from dataModules.database.db_tables import Products_Table

#
# [B] Initialisation de la session avec la base de données
#     Note : Nous sommes ici dans un programme 'classique' Python (non Flask)
#            En mode 'flask' : penser à paramétrer cette connection ET sa déconnection via l'objet 'g'
#                              comme lorsque nous faisons des ordres SQL Direct.
#

product_config = {
    'MAX_CONTENT_LENGTH': 1024 * 1024,
    'UPLOAD_EXTENSIONS': ['.jpg'],
    'UPLOAD_PATH': './static/uploads'
}

product_t = Products_Table()


class product:

    def __init__(self):
        self.data = {}

    def get_product_image(self, a_product):
        # print(quote(a_product['productLine']))
        # Est-ce que le produit a une image personnalisée ?
        # ie : il y a un fichier qui est = à son nom dans le répertoire upload

        fileToFind = os.path.join('static/uploads', f"{a_product['productCode']}.jpg")
        fileExist = os.path.exists(fileToFind)

        if (fileExist):
            return f"/static/uploads/{a_product['productCode']}.jpg"
        else:
            return f"https://loremflickr.com/320/240/{a_product['productLine']}"

    def top_5_products(self):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                Select p.productCode, p.productName, p.productLine, p.productDescription
                from products p
                order by p.quantityInStock desc
                limit 5
            """
            params = ()
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            # Pour chacun des produit trouvé, on va récupérer son 'image'
            # get_product_image s'occupe de tester et nous renvoyer si l'image a été uploadée ou
            # si il faut en prendre une générées aléatoirement.
            # On peut ensuite ajouter à notre 'dictionnaire' de résultat une entrée 'img' qui sera utilisée
            # par le template
            for record in records:
                record['productImg'] = self.get_product_image(record)

            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def get_for_carrousel(self):
        results = product_t.get_top_10()
        for result in results:
            if not os.path.exists(result['productImg']):
                result['productImg'] = f"https://loremflickr.com/320/240/{result['productCode']}"

        return results

    #
    #  Construire la liste de recherche
    #
    def search_product(self, search_criterias="", page=0):
        try:
            # print(len(search_criterias))
            # print(search_criterias)

            if len(search_criterias) == 0:
                return False

            cursor = g.connection.cursor(dictionary=True)

            #
            # Passe 1 : trouver le nombre de résultats (pour la pagination)
            #
            sql_select_query = f"""
                Select count(-1) nb
                from products p
                where p.productName like %s OR p.productDescription like %s
            """
            params = ('%{}%'.format(search_criterias), '%{}%'.format(search_criterias),)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()

            nb_result = records[0]['nb']

            #
            # Passe 2 : la requête avec la pagination
            #
            item_per_page = 10
            offset = (page - 1) * item_per_page

            sql_select_query = f"""
                Select *
                from products p
                where p.productName like %s OR p.productDescription like %s
                limit %s,%s
            """
            params = ('%{}%'.format(search_criterias), '%{}%'.format(search_criterias), offset, item_per_page,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()

            cursor.close()

            for record in records:
                record['productImg'] = self.get_product_image(record)

            return {
                "nbProducts": nb_result,
                "pagination": {
                    "nbPages": nb_result // item_per_page,
                    "curPage": page,
                    "route": "search",
                    "pageCriteria": f"&search={search_criterias}",
                },

                "products": records
            }

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def search_product_for_productLine(self, product_line="", page=1):
        try:
            cursor = g.connection.cursor(dictionary=True)

            if len(product_line) == 0:
                return False

            #
            # Passe 1 : trouver le nombre de résultats (pour la pagination)
            #
            sql_select_query = f"""
                Select count(-1) nb
                from products p
                where p.productLine = %s
            """
            params = (product_line,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()

            nb_result = records[0]['nb']

            #
            # Passe 2 : la requête avec la pagination
            #
            item_per_page = 10
            offset = (page - 1) * item_per_page

            sql_select_query = f"""
                Select *
                from products p
                where p.productLine = %s
                LIMIT %s, %s
                """
            params = (product_line, offset, item_per_page,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()

            cursor.close()

            for record in records:
                record['productImg'] = self.get_product_image(record)

            return {
                "nbProducts": nb_result,
                "pagination": {
                    "nbPages": nb_result // item_per_page,
                    "curPage": page,
                    "route": "productLine",
                    "productLine": product_line,
                    "pageCriteria": f"&productLine={product_line}",
                },
                "products": records
            }

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def nav_search_products_(self, search_criteria):
        dict_results = product_t.nav_search(search_criteria)
        results_temp = dict_results.get('products')
        results = [{}]
        j = 0
        for i in range(len(results_temp)):
            if not results[j].get('lineName'):
                results[j]['lineName'] = results_temp[i]['productLine']
                results[j]['products'] = []

            if results[j]['lineName'] != results_temp[i]['productLine']:
                results.append({'lineName': results_temp[i]['productLine'], 'products': []})
                j += 1

            if not os.path.exists(results_temp[i]['productImg']):
                results_temp[i]['productImg'] = f"https://loremflickr.com/320/240/{results_temp[i]['productCode']}"

            results[j]['products'].append({
                'productCode': results_temp[i]['productCode'],
                'productName': results_temp[i]['productName'],
                'productImg': results_temp[i]['productImg']
            })
        return {'products': results, 'nb_results': dict_results.get('nb_results')}

    # --------------------------
    # Opérations de mise à jour.

    def get_details(self, product_code):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                Select *
                from products p
                where p.productCode = %s
            """
            params = (product_code,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            if len(records) != 1:
                return False
            else:
                records[0]['productImg'] = self.get_product_image(records[0])
                return records[0]

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def get_product_scales(self):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                Select distinct productScale
                from products p
                order by productScale
            """
            params = ()
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            return [row['productScale'] for row in records]

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def get_product_vendors(self):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                Select distinct productVendor
                from products p
                order by productVendor
            """
            params = ()
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            return [row['productVendor'] for row in records]

        except Exception as e:
            print("Error reading data from MySQL table", e)

    #
    # Permet de valider si les information envoyées pour un produit sont correctes
    # (sert pour l'update ET l'insert
    # Renvoie : soit vrai si tout est ok, soit un dictionnaire des erreurs trouvées :
    #   {
    #       "nom_du_champ": ["Array des erreurs du champ", ...]
    #       ....
    #    }
    #
    def validate_product(self, request):

        errors = {}

        if "productCode" not in request.form:
            errors['productCode'] = "Le code produit est manquant."

        if "productCode" in request.form:
            if len(request.form['productCode']) > 15:
                errors['productCode'] = "Le code produit doit avoir au plus 15 caractères."

        if "productName" in request.form:
            if len(request.form['productName']) < 3:
                errors['productName'] = "Le nom du produit doit avoir au moins 3 caractères"

            if len(request.form['productName']) > 50:
                errors['productName'] = "Le nom du produit ne doit pas avoir plus de 50 caractères"
        else:
            errors['productName'] = "Pas de nom de produit"

        if "productLine" not in request.form:
            errors['productLine'] = "Pas de ligne produit."

        if "productVendor" not in request.form:
            errors['productVendor'] = "Pas de Vendeur de produit."

        if "productScale" not in request.form:
            errors['productScale'] = "Pas de taille de produit."

        if "productDescription" in request.form:
            if len(request.form['productDescription']) < 3:
                errors['productDescription'] = "La description du produit doit avoir au moins 3 caractères"

            if len(request.form['productDescription']) > 200:
                errors['productDescription'] = "La description du produit ne doit pas avoir plus de 300 caractères"
        else:
            errors['productDescription'] = "Pas de description de produit"

        if "buyPrice" in request.form:
            try:
                val = float(request.form['buyPrice'].replace(',', '.'))
            except ValueError:
                errors['buyPrice'] = "Le prix d'achat n'est pas numérique"
        else:
            errors['buyPrice'] = "Pas de prix d'achat"

        if "MSRP" in request.form:
            try:
                val = float(request.form['MSRP'].replace(',', '.'))
            except ValueError:
                errors['MSRP'] = "Le prix de vente n'est pas numérique"
        else:
            errors['MSRP'] = "Pas de prix de vente"

        if "quantityInStock" in request.form:
            try:
                val = int(request.form['quantityInStock'])
            except ValueError:
                errors['quantityInStock'] = "Les quantités doivent être un entier"

        if len(list(errors.keys())) > 0:
            return errors
        else:
            return False

    def update_product(self, request, productCode):
        #
        # Tests de cohésion côté serveur
        #
        errors = self.validate_product(request)

        #
        # Upload d'image !
        #
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            # On autorise que les images au format '.jpg'
            if file_ext not in product_config['UPLOAD_EXTENSIONS']:
                # abort(400)
                errors = {'imageFormat': "Le format de fichier doit être jpg"}
            else:
                # print(os.path.join(product_config['UPLOAD_PATH'], filename))
                uploaded_file.save(os.path.join(product_config['UPLOAD_PATH'], f'{productCode}.jpg'))

        if errors:
            #
            # On a des erreurs, on ne veut même pas faire d'update :)
            #
            return errors
        else:
            # Pas d'erreur ? On met à jour !
            print('Do update !')
            try:
                cursor = g.connection.cursor(dictionary=True)
                sql_select_query = f"""
                    update products
                        SET 
                        productName = %s,
                        productLine = %s,
                        productScale = %s,
                        productVendor = %s,
                        productDescription = %s,
                        quantityInStock = %s,
                        buyPrice = %s,
                        MSRP = %s
                    where productCode = %s
                """
                params = (
                    request.form['productName'],
                    request.form['productLine'],
                    request.form['productScale'],
                    request.form['productVendor'],
                    request.form['productDescription'],
                    int(request.form['quantityInStock']),
                    float(request.form['buyPrice'].replace(',', '.')),
                    float(request.form['MSRP'].replace(',', '.')),
                    request.form['productCode'],
                )
                cursor.execute(sql_select_query, params)
                cursor.close()
                g.connection.commit()

                # False = il n'y a pas d'erreur!
                return False

            except Exception as e:
                return {"general": e}

    def insert_product(self, request, productCode):

        #
        #
        # Tests de cohésion côté serveur
        #
        errors = self.validate_product(request)

        #
        # Upload d'image !
        #
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            # On autorise que les images au format '.jpg'
            if file_ext not in product_config['UPLOAD_EXTENSIONS']:
                # abort(400)
                errors = {'imageFormat': "Le format de fichier doit être jpg"}
            else:
                # print(os.path.join(product_config['UPLOAD_PATH'], filename))
                uploaded_file.save(os.path.join(product_config['UPLOAD_PATH'], f'{productCode}.jpg'))

        if errors:
            #
            # On a des erreurs, on ne veut même pas faire d'update :)
            #
            return errors
        else:
            # Pas d'erreur ? On met à jour !

            #
            # On vérifie que le product code n'exite pas déjà....
            #
            try:
                cursor = g.connection.cursor(dictionary=True)
                sql_select_query = f"""
                    Select productCode
                    from products p
                    where p.productCode = %s
                """
                params = (productCode,)
                cursor.execute(sql_select_query, params)
                records = cursor.fetchall()
                cursor.close()

                if len(records) == 1:
                    return {'productCode': 'Le code produit existe deja'}

            except Exception as e:
                print(e)
                return {'productCode': 'Erreur SQL'}

            print('Do insert !')
            try:
                cursor = g.connection.cursor(dictionary=True)
                sql_select_query = f"""
                    insert into products
                        (productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP)
                        VALUES 
                        (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                """
                params = (
                    productCode,
                    request.form['productName'],
                    request.form['productLine'],
                    request.form['productScale'],
                    request.form['productVendor'],
                    request.form['productDescription'],
                    int(request.form['quantityInStock']),
                    float(request.form['buyPrice'].replace(',', '.')),
                    float(request.form['MSRP'].replace(',', '.')),

                )
                cursor.execute(sql_select_query, params)
                cursor.close()
                g.connection.commit()

                # False = il n'y a pas d'erreur!
                return False

            except Exception as e:
                return {"general": e}

    def get_quote(self, products_in_cart):

        try:
            #
            # Passe 1 : on vérifie qu'on a bien la quantité en stock pour commander
            #

            product_keys = list(products_in_cart.keys())  # < Convertir Objet de type 'dictionnary Keys en liste

            format_strings = ','.join(['%s'] * len(product_keys))  # Générer autant de %s qu'il y a de product key

            cursor = g.connection.cursor(dictionary=True)

            if format_strings != '':
                sql_select_query = f"""
                    Select productCode, MSRP, quantityInStock, productName
                    from products p
                    where p.productCode IN (%s)
                """ % format_strings  # < permet d'injecter les %s que l'on a généré précédement.
                # params = (product_keys,) # On a déjà les product keys au format list
                cursor.execute(sql_select_query, product_keys)
                records = cursor.fetchall()
                cursor.close()

                # print(records)

                out_of_stock = {}
                in_stock = {}
                total_price = 0

                # On vérifie que chaque produit commandé est bien suffisament en stock
                for row in records:
                    if products_in_cart[row['productCode']]['quantity'] > row['quantityInStock']:
                        out_of_stock[row['productCode']] = {'productName': row['productName']}
                    else:
                        in_stock[row['productCode']] = {
                            'productName': row['productName'],
                            'quantity': products_in_cart[row['productCode']]['quantity'],
                            'price': row['MSRP'],
                        }
                        total_price = total_price + (row['MSRP'] * products_in_cart[row['productCode']]['quantity'])

                return {
                    'error': False,
                    'outOfStock': out_of_stock,
                    'inStock': in_stock,
                    'totalPrice': total_price
                }
            else:
                raise Exception("Il n'y a aucun produit dans le panier.")
        except Exception as e:
            return {"error": True, "message": str(e)}

    def place_order(self, products_in_cart, customer_id):
        try:
            cursor = g.connection.cursor(dictionary=True)

            print(products_in_cart)

            #
            # On génère la commande
            #
            # 1-Nouvel id de commande
            sql_select_query = f"""
                Select max(orderNumber) + 1 orderNumber
                from orders
            """
            cursor.execute(sql_select_query)
            records = cursor.fetchall()
            order_number = records[0]['orderNumber']

            print(order_number)

            # 2-Commande
            sql_select_query = f"""
                Insert into orders (orderNumber, orderDate, requiredDate, status, customerNumber)
                Values (%s, now(), now(), 'Placed', %s)
            """
            params = (order_number,
                      customer_id
                      )
            cursor.execute(sql_select_query, params)

            # 3-Détail de la commande
            order_line_number = 0
            for key in products_in_cart:
                print(products_in_cart[key])
                sql_select_query = f"""
                    Insert into orderdetails (orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber)
                    Values (%s, %s, %s, %s, %s)
                """
                params = (
                    order_number,
                    products_in_cart[key]['productCode'],
                    products_in_cart[key]['quantity'],
                    products_in_cart[key]['price'],
                    order_line_number,
                )
                cursor.execute(sql_select_query, params)
                order_line_number += 1

            cursor.close()
            g.connection.commit()

            return {"error": False, "orderDetails": {"orderNumber": order_number}}

        except Exception as e:
            print(e)
            return {"error": True, "message": str(e)}
