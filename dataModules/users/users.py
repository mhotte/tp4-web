import hashlib

from flask import g, session


class user:

    def login(self, request):
        error = False
        #
        # On vérifie déjà que le compte existe déjà ?
        #

        username = request.form['username']

        #
        # Le mot de passe qui a été saisi par l'internaute
        #
        # Récupérer le mot de passe et le convertir en UTF8 (pour le hashage)
        password = request.form['password'].encode('utf-8')
        # Le hash du password, converti en hexadécimal + mis en majuscule (pour le comparer à mon propre hash)
        hash_password = hashlib.md5(password).hexdigest().upper()

        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                Select u.username, customer_id, employee_id
                from users u
                Where u.username = %s and u.password = %s                
            """
            params = (username, hash_password)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            #
            # On a un enregistrement... l'utilisateur existe déjà....
            #
            if not len(records) == 1:
                error = {'username': 'Utilisateur invalide'}
            else:
                if records[0]['customer_id']:
                    return {'error': False, 'is_customer': True, 'is_employee': False, 'user_id': records[0]['customer_id'], 'user_name': username}
                elif records[0]['employee_id']:
                    return {'error': False, 'is_customer': False, 'is_employee': True, 'user_id': records[0]['employee_id'], 'user_name': username}
                else:
                    error = {'general': f"Ni employee, ni costumer ?"}

        except Exception as e:
            error = {'general': f"A.Error reading data from MySQL table {e}"}

        return {'error': True, 'errors': error}

    def create(self, request):
        error = False

        username = request.form['username']
        password_a = request.form['passwordA'].encode('utf-8')
        password_b = request.form['passwordB'].encode('utf-8')

        if len(username) == 0:
            error = {'username': 'Utilisateur vide'}
        else:
            # On vérifie déjà que le compte n'existe pas déjà....
            try:
                cursor = g.connection.cursor(dictionary=True)
                sql_select_query = f"""
                    Select u.username
                    from users u
                    Where u.username = %s                
                """
                params = (username,)
                cursor.execute(sql_select_query, params)
                records = cursor.fetchall()
                cursor.close()

                #
                # On a un enregistrement... l'utilisateur existe déjà....
                #
                if len(records) == 1:
                    error = {'username': 'Cet utilisateur existe déjà'}

                else:
                    # Est-ce que les deux mots de passe sont identiques ?
                    if password_a != password_b:
                        error = {'password': 'Utilisateur vide'}
                    else:
                        #
                        # Ok! On va créer le nouveau customer (et générer un customerNumber)
                        #
                        # 1. Cette table n'a pas d'id automatique, on va donc d'abord chercher le max id +1
                        #    Comme nous avons une transaction en cours avec la base de données,
                        #    même si une autre personne essais d'insérer au même moment nous aurons jamais le même id.
                        #    Mais la bonne manière de mitiger ça est de mettre soit des id auto incrémentaux - soit des
                        #    sequences (Oracle).
                        #
                        try:

                            # Passe en mode 'transactions'
                            g.connection.autocommit = False

                            cursor = g.connection.cursor(dictionary=True)

                            sql_select_query = f"""
                                Select max(customerNumber) + 1 newId
                                from customers
                            """
                            params = ()
                            cursor.execute(sql_select_query, params)
                            records = cursor.fetchall()

                            newCustomerId = records[0]['newId']

                            #
                            # Ok! On peut faire l'insert du nouvel utilisateur!
                            #
                            hash_password = hashlib.md5(password_a).hexdigest().upper()
                            sql_select_query = f"""
                                INSERT INTO users(username, password, customer_id, employee_id) 
                                 VALUES(%s, %s, %s, null);
                            """
                            params = (username, hash_password, newCustomerId,)
                            cursor.execute(sql_select_query, params)

                            sql_select_query = f"""
                                INSERT INTO customers(
                                    customerNumber,
                                    customerName,
                                    contactLastName,
                                    contactFirstName,
                                    addressLine1,
                                    addressLine2,
                                    city,
                                    state,
                                    postalCode,
                                    country,
                                    salesRepEmployeeNumber,
                                    creditLimit,
                                    phone
                                )
                                 VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, null, 0, '')
                            """
                            params = (
                                newCustomerId,
                                request.form['username'],
                                request.form['lastName'],
                                request.form['firstName'],
                                request.form['addressLine1'],
                                request.form['addressLine2'],
                                request.form['city'],
                                request.form['state'],
                                request.form['postalCode'],
                                request.form['country'],
                            )
                            cursor.execute(sql_select_query, params)
                            #

                            g.connection.commit()
                            cursor.close()

                            #
                            # Sortie 'succès' : l'utilisateur est unique, et le customer a bien été créé
                            # On met le nouveau user_id directement ici:
                            session['user_id'] = newCustomerId
                            return False

                        except Exception as e:
                            print(e)
                            g.connection.rollback()
                            error = {'general': f".Error reading data from MySQL table {e}"}
            except Exception as e:
                error = {'general': f"A.Error reading data from MySQL table {e}"}
        # Sortie en cas d'erreur, a ramassé tous les messages d'erreur.
        return error
